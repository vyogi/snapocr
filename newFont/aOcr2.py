# performs ocr on a specific image type
import cv2
import numpy as np
from PIL import Image
import os,sys,time,re
from retry import clean_misdDot
import sqlite3

from docx import Document
from docx.shared import Pt

#debug = True
debug = False

line_break_debug = False
conn = sqlite3.connect('./dbTemplate4/keys.db')


def parse_jpg2(myfile):
	
	if(not os.path.isfile(myfile)):
		print myfile
		sys.exit("Invalid filepath");
	
	## is file a JPG?
	if(not myfile.lower().endswith('.jpg')):
		return "Invalid filetype. Current version is limited to jpg file processing.";
	
	img = cv2.imread(myfile,0)

	start_time = time.time()
	
	#Converting image to binary form
	ret, th1 = cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	
	#Convert the image array to a numpy array
	im1arr = np.array(th1)
	
	
	img = Image.fromarray(th1)
	img.save(myfile[:-4]+'.bmp');
	# sys.exit(0)
	#fetching image dimentions
	rows, cols = im1arr.shape
	
	# print im1arr
	if debug:
		print ("Image dimensions are %d x %d"%(rows, cols))	
	
	all_lines = []
	line = 0;
	black_pixel_flag = 0;
	prev_region = 0;
	this_region = 0;
	
	for x in range(rows):
		black_pixel_flag=0;
		for y in range(cols):
			if im1arr[x][y]== 0:			# Matches val == 0 of a black pixel
				black_pixel_flag=1;
		if black_pixel_flag == 0: 			# and white_region == 0:#we have a line
			this_region = 0;
		else:
			this_region = 1;

		if prev_region != this_region:
			line = line + 1
			all_lines.append(x)
			prev_region = this_region
	
	##Word segmentation
	
	
	words=0
	all_words=[]
	x_comp=[]
	i=0
	black_pixel_flag=0;
	prev_region=0;
	this_region=0;
	li_len = line/2
	fChange=0;
	
	while li_len:
		fChange=0;
		for y in range(cols):
			
			black_pixel_flag=0;
			for x in range(all_lines[i],all_lines[i+1]):
				if im1arr[x][y] == 0:			# we have got a black pixel
					black_pixel_flag=1;
			
			if black_pixel_flag == 0: 			# and white_region == 0:	#we have a line
				this_region = 0;
			else:
				this_region = 1;
				if fChange == 0:
					x_comp.append(y)
					fChange = 1


			if prev_region != this_region:
				words = words + 1
				all_words.append(y)
				prev_region = this_region
				
		x_comp.append(all_words[-1])
		i+=2
		li_len-=1
	
	##print x_comp
	##Final Plotting
	j=0
	
	var=''
	lineW = 0
	prevLineMark = 0

	print ("we've got %d lines"%(line/2))

	for i in range(line/2):
		
		x0 = x_comp[j]-1
		x1 = x_comp[j+1]+1
		y0 = all_lines[j]-2
		y1 = all_lines[j+1]+1
		if debug:
			print "co-ordinates"
			print x0,x1,y0,y1
			
		area  =(x1-x0)*(y1-y0-1)
		
		if not area<50:
			lineW = y1 - prevLineMark
			prevLineMark = y1
			simg = im1arr[y0:y1,x0:x1]
			# cv2.namedWindow('ParsedArray', cv2.CV_WINDOW_AUTOSIZE)
			# cv2.imshow('ParsedArray',simg)
			# cv2.waitKey(0)
			# cv2.destroyAllWindows()
			#print ""
			#print "Parsing this Line"
			#plt.imshow(simg)
			#plt.show()
			#tvar = "\n"
			tvar = ""
			tvar += do_image_compare_and_store( simg ) ##We have just passed a complete line to the parser
			
			#tvar += '\n'
			#print lineW
			
			#print tvar
			#var = 'XXXXXXXX\n'
			mFlag = False
			if i:
				for k in xrange(lineW/25):					
					var += '\n'
					mFlag = True
			
			if not mFlag:
				var += '\n'

			#var = var + str(i) +' : '+ tvar
			var = var + tvar
			if line_break_debug:
				print var.decode('unicode_escape')
				raw_input("Press Enter to continue...")
		j=j+2
	##Create a docx file
	document = Document('outline.docx');
	run = document.add_paragraph().add_run()
	font = run.font
	font.size = Pt(8)
	
	nvar = ''
	nvar = clean_misdDot(var)
	var = re.sub('Countr[^a-z]\s','country',nvar)
	nvar = re.sub('yy','y',var)
	var = re.sub('HH','H',nvar)

	document.add_paragraph(var.decode('unicode_escape'))
	document.save(myfile[:myfile.index('.')]+'.docx')
	#print var.decode('unicode_escape')
	#if debug:
		#print("--- %s seconds ---" % (time.time() - start_time))
	
	return var
	#return (time.time() - start_time)
	

def chunk_and_process(im1arr):
	
	
	rows, cols = im1arr.shape
	
	if debug:
		print "\r\nFCall #chunk_and_process:"
		print("Current Row count: %d, Column count: %d"% (rows, cols))
	
	ans=''
	words=0
	prev_col=0
	prev_coll=0
	all_words=[]
	prev_region=0
	space = 0
	for y in range(cols):
		black_pixel_flag=0;
		for x in range(rows):
			if im1arr[x][y] == 0:			# we have got a black pixel
				black_pixel_flag=1;

		if black_pixel_flag == 0: 			# and white_region == 0:	#we have a line
			this_region = 0;
		else:
			this_region = 1;


		if prev_region != this_region:
			#plt.imshow(im1arr[:,prev_col:y])
			#plt.show()
			#	READ_MATCH TEMPLATE HERE 
			nu_arr = im1arr[:,prev_col:y]
			h = rows
			w = y - prev_coll
			#print("w is %d, h is %d"%(w,h))
			if(black_pixel_flag and w<2):
				#####print "----x----"
				#all_words.append(y)
				#prev_col = y
				#if words:
				#	all_words.remove(prev_col)
				prev_region = this_region
				continue
			if(black_pixel_flag and w>=2):
				# plt.imshow(im1arr[:, prev_col:prev_coll])
				# plt.show()
				if(prev_col):
					str = do_image_compare_and_store(im1arr[:, prev_col-1:prev_coll])
				else:
					str = do_image_compare_and_store(im1arr[:, prev_col:prev_coll])
					
				ans+=str
				#print("w is %d, h is %d"%(w,h))
				#print "---space--"
				ans += ' '
				space+=1
				words = words + 1
				all_words.append(y)
				prev_col = y
				prev_region = this_region
				continue
			#Lets see if its a space
			
			
			# if(black_pixel_flag and (w > 3) and (w < 7)):#white area with sufficient length
				# if words:	#This is not the first word in line
					# ans += ' '
			# else:
				# if(black_pixel_flag and (w > 7)):#white area with sufficient length
					# if words:	#This is not the first word in line
						# ans += '\t'	
						# continue
			#Matching Begins. First Word Matches
			
			
			
			# ans += str
			
			# PASS 1 Lets start by first character 
			# Whats the max width that we can expect? 
			# We'll rather do it big 2 small 
			
			
			all_words.append(y)
			prev_coll = y
			prev_region = this_region
	
	str = do_image_compare_and_store(im1arr[:, prev_col:prev_coll])
	

	ans += str
	
	
	if debug:
		print ("%d spaces found"%(space))
		print("Parsed %d, words"%(words+1))
	return ans

def misd_dot( nstr ):
	val = re.finditer(r'[a-z]\s[A-Z]',nstr)
	cnt = 0
	#val = re.search("[a-z]\s[A-Z]", str)#.start() #re.sub('[a-z]\s[A-Z]','[a-z]\.\s[A-Z]',str)
	vals = [ m.start(0) for m in val]
	for i in vals:
		cnt = cnt + 1
		nstr = nstr[:i+cnt]+'.'+nstr[i+cnt:]
	return nstr

def un_space(nstr):
	val = re.finditer(r'[A-Z]\s[a-z]',nstr)
	vals = [ m.start(0) for m in val]
	cnt = 0

	for i in vals:
		charr = nstr[i-cnt:i-cnt+1]
		
		if charr != "I":		
			nstr = nstr[:i+1-cnt]+nstr[i+2-cnt:]
			cnt = cnt + 1
		
	return nstr

def do_image_compare_and_store(wrd_img):
	match_count = 0
	#plt.imshow(wrd_img)
	#plt.show()
	if debug:
		print("\r\nNew Line:")
	h,w = wrd_img.shape[:2]
	# cursor = conn.execute("SELECT width,cdata,path from charmap WHERE width BETWEEN ? AND ? ORDER BY width",(w-2,w))	
	# result=0
	# str = 0
	tAns=""
	# match_count = 0
	# for row in cursor:
		# template = cv2.imread(row[2],0)
		# hh,ww = template.shape[:2]

		# ##If template height is greater than image DON'T DO MATCHING
		# if (hh > h) or (ww > w):
			# continue
		# localtime = time.asctime( time.localtime(time.time()) )
		# logging.debug(localtime+ 'comparing with '+row[2])
		
		# #print row[2]
		# ##result = cv2.matchTemplate(nu_arr,template, cv2.TM_CCOEFF)
		# ##imgar = Image.fromarray(nu_arr)
		# result = cv2.matchTemplate(wrd_img, template, cv2.TM_CCORR_NORMED)
		# th, tw = template.shape[:2]
		# ##minVal,maxVal,minLoc,maxLoc = cv.MinMaxLoc(result)
		# ##decide if we have a matching template
		# threshold = 0.97
		# loc = np.where(result >= threshold)
		# match_count = 0
		# for pt in zip(*loc[::-1]):
			# cv2.rectangle(wrd_img, pt, (pt[0] + tw-1, pt[1] + th-2), (255,255,255), -1)
			# match_count+=1
		# if match_count:
			# str = row[1]
			# tAns = str
			# break
		
	if not match_count:
		##lets get this template in database
		#word_match()
		#if debug:
		#	str = store_template(wrd_img)
		#else:
		#	str = 'unknown'
		
		##################################
		##			BOMB SITE HERE		##
		##################################
		cursor = conn.execute("SELECT width,cdata,path,ustat from apfamap order by ROWID")
		result=0
		str = []
		match_count = 0
		pSet=[]
		cnt = 0
		for row in cursor:
			# cnt = cnt + 1
			# print cnt
			template = cv2.imread(row[2],0)
			hh,ww = template.shape[:2]

			##If template height is greater than image DON'T DO MATCHING
			if (hh > h) or (ww > w):
				continue
			
			result = cv2.matchTemplate(wrd_img, template, cv2.TM_CCORR_NORMED)
			if row[3] == 0:
				threshold = 0.96
			else:
				threshold = row[3]/100.0
			
			loc = np.where(result >= threshold)
			match_count = 0
			for pt in zip(*loc[::-1]):
				#cv2.rectangle(wrd_img, pt, (pt[0] + tw-1, pt[1] + th-2), (255,255,255), -1)
				str.append( row[1])
				if debug:
					print ("\t\t\t %s @ %d ,%d  :%s"%(row[1].decode('unicode_escape'),pt[0],pt[1],row[2]))
				match_count+=1
				pSet.append(pt[0])
				#print ("Using threshold:%f , %d, %f"%(threshold, row[3], row[3]/100.0))
				
				
			
		rnew = pSet[:]
		pSet.sort()
		#print pSet
		#print rnew
		lval = 0
		
			
		for i in range(len(rnew)):
			#we need to make sure that we are not overwriting the same value
			if((pSet[i]-lval <= 3) and i>0):
				#compare old and current characters
				if (str[rnew.index(pSet[i])] == str[rnew.index(lval)]):
					#its the same character
					#print("%s @ %d matches %s @ %d"%(str[rnew.index(pSet[i])],pSet[i],str[rnew.index(lval)],lval))
					continue
			
			tAns = tAns + str[rnew.index(pSet[i])]
			lval = pSet[i]
		
		#values = [i.start() for i in re.finditer(re.escape('dl '),tAns)]
		#nAns = re.sub('T\sh ','Th',tAns)
		#nAns = re.sub('dld','dd ',tAns)
		nAns = re.sub('T\sh','Th',tAns)
		#tAns = re.sub('dl\.','d.',nAns)
		#tAns = re.sub('[a-z]\s[A-Z]','',nAns)
		tAns = re.sub('\s\s',' ',nAns)
		nAns = misd_dot(tAns)
		tAns = re.sub('hl','h',nAns)
		nAns = re.sub(',;',';',tAns)

		nAns = re.sub('[a-z]\s[A-Z]|[a-z]\n[A-Z]',';',tAns)
		tAns = un_space(nAns)
		
		return tAns
		#return tAns
	else:
		return tAns
		
	return '?undef'

def inii():
	logging.basicConfig(filename='./Log/ocr.log',level=logging.DEBUG)
	localtime = time.asctime( time.localtime(time.time()) )
	logging.debug('Date is '+localtime)
	logging.debug('database_connected')
	
def store_template(templt):
	plt.imshow(templt)
	plt.show()
	n= raw_input("Please enter character(s) in this image:")
	print ('you entered: %s'%(n.decode('unicode_escape')))
	#print ('you entered: %s'%(n))
	#lets save it in our database
	#####get the width
	h, w = templt.shape[:2]
	print ('Template Width: %d Height:%d'%(w,h))
	cursor = conn.execute('SELECT max(rowid) FROM charmap')
	max_id = cursor.fetchone()[0]
	#Dprint str(int(max_id or 0)+1)
	#Dexit(0)
	tem_path='./dbTemplate/key/temp_'+str(int(max_id or 0)+1)+'.bmp'
	img = Image.fromarray(templt)
	img.save(tem_path)
	conn.execute('insert into charmap (width,cdata,path) values(?,?,?)',(w,n,tem_path))
	conn.commit()
	localtime = time.asctime( time.localtime(time.time()) )
	# logging.debug(localtime+ 'created new template: '+tem_path)
	print('template successfully saved in memory')
	return n
	
if __name__ == "__main__":
	'''this file is to be imported'''
	print '''
This python module should not be called directly.
common usage:
>> from aOcr2 import parse_jpg2
>> parse_jpg2("doc_jpg_file.jpg")'''
