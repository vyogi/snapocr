import sqlite3
import re

## the db file should be visible to this script
conn = sqlite3.connect('keys.db')


def rectify_db():
	count_r = 0
	new_Str = ''
	cursor = conn.execute('SELECT path  FROM apfamap order by ROWID')
	for row in cursor:
		#print row[0]
		if 'dbTemplate3' in row[0]:
			pcopy = row[0]
			new_Str = re.sub('dbTemplate3','dbTemplate4',pcopy)
			conn.execute("UPDATE apfamap set path=(?) where path=(?)", (new_Str, row[0]))
			conn.commit()
			count_r += 1


	return count_r

if __name__ == "__main__":
	'''this file intends to rectify the folder/directory 
	name stored in database entries'''
	
	r_count =  rectify_db()
	print 'Done ', r_count, 'rectifications'