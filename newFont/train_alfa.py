# performs ocr on a specific image type
import cv2
import numpy as np
from PIL import Image
import os,sys,time
from matplotlib import pyplot as plt
import sqlite3
import logging


PATH = "./"

debug = True
conn = sqlite3.connect(PATH+'dbTemplate3/keys.db')



def do_train(myfile):
	inii()
	if(not os.path.isfile(myfile)):
		print myfile
		sys.exit("Invalid filepath");
	
	## is file a JPG?
	## 
	if(not myfile.lower().endswith('.bmp')):
		sys.exit("Invalid filetype. A bmp file is required for training.");
	
	img = cv2.imread(myfile,0)
	
	
	start_time = time.time()
	

	
	im1arr = np.array(img)
	

	rows, cols = im1arr.shape

	if debug:
		print ("Image dimensions are %d x %d"%(rows, cols))
	all_lines=[]
	line=0;
	black_pixel_flag=0;
	prev_region=0;
	this_region=0;
	
	for x in range(rows):
		black_pixel_flag=0;
		for y in range(cols):
			if im1arr[x][y]== 0:			# Matches val == 0 of a black pixel
				black_pixel_flag=1;
		if black_pixel_flag == 0: 			# and white_region == 0:	#we have a line
			this_region = 0;
		else:
			this_region = 1;

		if prev_region != this_region:
			line = line + 1
			all_lines.append(x)
			prev_region = this_region
	
	##Word segmentation
	
	
	words=0
	all_words=[]
	x_comp=[]
	i=0
	black_pixel_flag=0;
	prev_region=0;
	this_region=0;
	li_len = line/2
	fChange=0;
	
	while li_len:
		fChange=0;
		for y in range(cols):
			
			black_pixel_flag=0;
			for x in range(all_lines[i],all_lines[i+1]):
				if im1arr[x][y] == 0:			# we have got a black pixel
					black_pixel_flag=1;
			
			if black_pixel_flag == 0: 			# and white_region == 0:	#we have a line
				this_region = 0;
			else:
				this_region = 1;
				if fChange == 0:
					x_comp.append(y)
					fChange = 1


			if prev_region != this_region:
				words = words + 1
				all_words.append(y)
				prev_region = this_region
				
		x_comp.append(all_words[-1])
		i+=2
		li_len-=1
	
	##print x_comp
	##Final Plotting
	j=0
	for i in range(line/2):
		x0 = x_comp[j]-1
		x1 = x_comp[j+1]+1
		y0 = all_lines[j]-2
		y1 = all_lines[j+1]+1
		if debug:
			print "co-ordinates"
			print x0,x1,y0,y1
		
		area  =(x1-x0)*(y1-y0-1)
		if area<50:
			cv2.rectangle(img,(x0,y0), (x1,y1), 255, -1)
		else:
			cv2.rectangle(img,(x0,y0), (x1,y1), (0,0,255), 1)
		

		#continue
		#if not area<25:
			# pts1 = np.float32([[x0+4,y0],[x1+4,y0],[x0,y1],[x1,y1]])
			# pts2 = np.float32([[x0,y0],[x1,y0],[x0,y1],[x1,y1]])
			# M = cv2.getPerspectiveTransform(pts1,pts2)
			
			# simg = im1arr[y0:y1,x0:x1]
			
			# dst = cv2.warpPerspective(simg,M,(x1-x0,y1-y0))
			# imgg = Image.fromarray(th1)
			# imgg.save('tempy.bmp')
			# sys.exit(0)
		simg = im1arr[y0:y1,x0:x1]
		var = chunk_and_process( simg ) ##We have just passed a complete line to the parser
		print var
			#print "slideshow"
			#plt.subplot(211),plt.imshow(simg),plt.title('Input')
			#plt.subplot(212),plt.imshow(th2),plt.title('Output')
			#plt.show()
		# # # k = cv2.waitKey(20) & 0xFF
		# # # if k == 27:
			# # # break
		j=j+2
	
	print("--- %s seconds ---" % (time.time() - start_time))
	plt.imshow(img,cmap = 'gray')
	plt.show()
	
	return "success"
	

def chunk_and_process(im1arr):
	
	
	rows, cols = im1arr.shape
	
	if debug:
		print "\r\nFCall #chunk_and_process:"
		print("Current Row count: %d, Column count: %d"% (rows, cols))
	if (rows==0 or cols==0):
		return ''
	ans=''
	words=0
	prev_col=0
	prev_coll=0
	all_words=[]
	prev_region=0
	space = 0
	for y in range(cols):
		black_pixel_flag=0;
		for x in range(rows):
			if im1arr[x][y] == 0:			# we have got a black pixel
				black_pixel_flag=1;

		if black_pixel_flag == 0: 			# and white_region == 0:	#we have a line
			this_region = 0;
		else:
			this_region = 1;


		if prev_region != this_region:
			#plt.imshow(im1arr[:,prev_col:y])
			#plt.show()
			#	READ_MATCH TEMPLATE HERE 
			nu_arr = im1arr[:,prev_col:y]
			h = rows
			w = y - prev_coll
			#print("w is %d, h is %d"%(w,h))
			if(black_pixel_flag and w<2):
				print "----x----"
				#all_words.append(y)
				#prev_col = y
				#if words:
				#	all_words.remove(prev_col)
				prev_region = this_region
				continue
			if(black_pixel_flag and w>=2):
				# plt.imshow(im1arr[:, prev_col:prev_coll])
				# plt.show()
				str = do_image_compare_and_store(im1arr[:, prev_col:prev_coll])
				ans+=str
				#print("w is %d, h is %d"%(w,h))
				#print "---space--"
				ans += ' '
				space+=1
				words = words + 1
				all_words.append(y)
				prev_col = y
				prev_region = this_region
				continue
			#Lets see if its a space
			
			
			# if(black_pixel_flag and (w > 3) and (w < 7)):#white area with sufficient length
				# if words:	#This is not the first word in line
					# ans += ' '
			# else:
				# if(black_pixel_flag and (w > 7)):#white area with sufficient length
					# if words:	#This is not the first word in line
						# ans += '\t'	
						# continue
			#Matching Begins. First Word Matches
			
			
			
			# ans += str
			
			# PASS 1 Lets start by first character 
			# Whats the max width that we can expect? 
			# We'll rather do it big 2 small 
			
			
			all_words.append(y)
			prev_coll = y
			prev_region = this_region
	
	str = do_image_compare_and_store(im1arr[:, prev_col:prev_coll])
	ans += str
	
	
	if debug:
		print ("%d spaces found"%(space))
		print("Parsed %d, words"%(words+1))
	return ans


def do_image_compare_and_store(wrd_img):
	#plt.imshow(wrd_img)
	#plt.show()
	
	
	h,w = wrd_img.shape[:2]
	cursor = conn.execute("SELECT width,cdata,path from apfamap order by ROWID")	
	result=0
	str = 0
	match_count = 0
	for row in cursor:
		template = cv2.imread(PATH + row[2],0)
		hh,ww = template.shape[:2]

		##If template height is greater than image DON'T DO MATCHING
		if (hh > h) or (ww > w):
			continue
		localtime = time.asctime( time.localtime(time.time()) )
		logging.debug(localtime+ 'comparing with '+row[2])
		
		#print row[2]
		##result = cv2.matchTemplate(nu_arr,template, cv2.TM_CCOEFF)
		##imgar = Image.fromarray(nu_arr)
		result = cv2.matchTemplate(wrd_img, template, cv2.TM_CCORR_NORMED)
		th, tw = template.shape[:2]
		##minVal,maxVal,minLoc,maxLoc = cv.MinMaxLoc(result)
		##decide if we have a matching template
		threshold = 0.97
		loc = np.where(result >= threshold)
		match_count = 0
		for pt in zip(*loc[::-1]):
			cv2.rectangle(wrd_img, pt, (pt[0] + tw-1, pt[1] + th-2), (255,255,255), -1)
			match_count+=1
		if match_count:
			str = row[1]
			break
		
	if not match_count:
		##lets get this template in database
		
		if debug:
			str = store_template(wrd_img)
		else:
			str = 'unknown'
		return str
	else:
		return str
		
	return 'strr'

def inii():
	logging.basicConfig(filename='./Log/ocr.log',level=logging.DEBUG)
	localtime = time.asctime( time.localtime(time.time()) )
	logging.debug('Date is '+localtime)
	logging.debug('database_connected')
	
def store_template(templt):
	plt.imshow(templt)
	plt.show()
	n= raw_input("Please enter character(s) in this image:")
	print ('you entered: %s'%(n.decode('unicode_escape')))
	#print ('you entered: %s'%(n))
	#lets save it in our database
	#####get the width
	h, w = templt.shape[:2]
	print ('Template Width: %d Height:%d'%(w,h))

	cursor = conn.execute('SELECT max(rowid) FROM apfamap')
	max_id = cursor.fetchone()[0]
	#Dprint str(int(max_id or 0)+1)
	#Dexit(0)
	tem_path = './dbTemplate3/key2/temp_'+str(int(max_id or 0)+1)+'.bmp'
	if w>1:
		img = Image.fromarray(templt[:,1:]) 	## 18-6-2015 . No need  for image refinement/snipping
	else:
		img = Image.fromarray(templt) 	
	
	img.save(tem_path)
	conn.execute('insert into apfamap (width,cdata,path) values(?,?,?)',(w,n,tem_path))
	conn.commit()
	localtime = time.asctime( time.localtime(time.time()) )
	logging.debug(localtime+ 'created new template: '+tem_path)
	print('template successfully saved in memory')
	return n

if __name__ == "__main__":
	'''Call the training program for action'''
	#do_train("train.bmp")
	do_train("mTrain.bmp")