from brne import do_matching, get_brne_mat

def get_digit_str(thisfile):
	brn = 0
	ext_img = get_brne_mat(thisfile)
	brn = do_matching(ext_img)
	return brn