#snapParser.py
import cv2
import numpy as np
from PIL import Image
import sqlite3, os, sys, time, re




DB_PATH = "dbTemplate"
#PATH = "/home/vvy/ocrServe/Log/"
PATH = './'

conn = ""
#line_break_debug = True
line_break_debug = False
debug = False
ffamily = 0

def parse_data( fontfamily, myfile ):
	global DB_PATH
	global conn
	global PATH
	global ffamily

	DB_PATH = DB_PATH + str(fontfamily)	
	ffamily = fontfamily
	conn = sqlite3.connect(PATH + DB_PATH + '/keys.db')
	img = cv2.imread(PATH+myfile,0)

	#print PATH+myfile
	## converting image to binary form
	ret, th1 = cv2.threshold(img[:-50,:],0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	#ret, th1 = cv2.threshold(img,120,255,cv2.THRESH_BINARY)
	#Convert the image array to a numpy array
	im1arr = np.array(th1)
	cv2.imwrite(myfile[:-4]+'.bmp', th1)

	print ("Parsing file : %s"%(myfile))
	#fetching image dimentions
	#print im1arr.shape
	rows, cols = im1arr.shape
	##rows = rows-50

	##################################################
	##												##
	## here begins the processor intensive task		##
	##												##
	##################################################
	all_lines = []
	line = 0;
	black_pixel_flag = 0;
	prev_region = 0;
	this_region = 0;
	
	for x in range(rows):
		black_pixel_flag=0;
		for y in range(cols):
			if im1arr[x][y]== 0:			# Matches val == 0 of a black pixel
				black_pixel_flag=1;
		if black_pixel_flag == 0: 			# and white_region == 0:#we have a line
			this_region = 0;
		else:
			this_region = 1;

		if prev_region != this_region:
			line = line + 1
			all_lines.append(x)
			prev_region = this_region
	
	##Word segmentation
	
	
	words=0
	all_words=[]
	x_comp=[]
	i=0
	black_pixel_flag=0;
	prev_region=0;
	this_region=0;
	li_len = line/2
	fChange=0;
	
	while li_len:
		fChange=0;
		for y in range(cols):
			
			black_pixel_flag=0;
			for x in range(all_lines[i],all_lines[i+1]):
				if im1arr[x][y] == 0:			# we have got a black pixel
					black_pixel_flag=1;
			
			if black_pixel_flag == 0: 			# and white_region == 0:	#we have a line
				this_region = 0;
			else:
				this_region = 1;
				if fChange == 0:
					x_comp.append(y)
					fChange = 1


			if prev_region != this_region:
				words = words + 1
				all_words.append(y)
				prev_region = this_region
				
		x_comp.append(all_words[-1])
		i+=2
		li_len-=1
	
	##print x_comp
	##Final Plotting
	j=0
	
	var=''
	lineW = 0
	prevLineMark = 0
	if debug:
		print ("we've got %d lines"%(line/2))

	for i in range(line/2):
		
		x0 = x_comp[j]-1
		x1 = x_comp[j+1]+1
		y0 = all_lines[j]-2
		y1 = all_lines[j+1]+1
		if debug:
			print "co-ordinates"
			print x0,x1,y0,y1
			
		area  =(x1-x0)*(y1-y0-1)
		
		if not area<50:
			lineW = y1 - prevLineMark
			prevLineMark = y1
			simg = im1arr[y0:y1,x0:x1]
			# cv2.namedWindow('ParsedArray', cv2.CV_WINDOW_AUTOSIZE)
			# cv2.imshow('ParsedArray',simg)
			# cv2.waitKey(0)
			# cv2.destroyAllWindows()
			#print ""
			#print "Parsing this Line"
			#plt.imshow(simg)
			#plt.show()
			tvar = ""
			tvar += do_image_compare_and_store( simg ) ##We have just passed a complete line to the parser
			
			#tvar += '\n'
			#print lineW
			
			#print tvar
			#var = ""
			if i:
				for i in xrange(lineW/25):					
					var += '\n'
			var = var + tvar
			if line_break_debug:
				print var.decode('unicode_escape')
				raw_input("Press Enter to continue...")
		j=j+2
	return var

def misd_dot( nstr ):
	val = re.finditer(r'[a-z]\s[A-Z]',nstr)
	cnt = 0
	#val = re.search("[a-z]\s[A-Z]", str)#.start() #re.sub('[a-z]\s[A-Z]','[a-z]\.\s[A-Z]',str)
	vals = [ m.start(0) for m in val]
	for i in vals:
		cnt = cnt + 1
		nstr = nstr[:i+cnt]+'.'+nstr[i+cnt:]
	return nstr

def un_space(nstr):
	val = re.finditer(r'[A-Z]\s[a-z]',nstr)
	vals = [ m.start(0) for m in val]
	cnt = 0

	for i in vals:
		charr = nstr[i-cnt:i-cnt+1]
		
		if charr != "I":		
			nstr = nstr[:i+1-cnt]+nstr[i+2-cnt:]
			cnt = cnt + 1
		
	return nstr

def do_image_compare_and_store(wrd_img):
	match_count = 0
	global DB_PATH
	#plt.imshow(wrd_img)
	#plt.show()
	if debug:
		print("\r\nNew Line:")
	h,w = wrd_img.shape[:2]
	# cursor = conn.execute("SELECT width,cdata,path from charmap WHERE width BETWEEN ? AND ? ORDER BY width",(w-2,w))	
	# result=0
	# str = 0
	tAns=""
	# match_count = 0
	# for row in cursor:
		# template = cv2.imread(row[2],0)
		# hh,ww = template.shape[:2]

		# ##If template height is greater than image DON'T DO MATCHING
		# if (hh > h) or (ww > w):
			# continue
		# localtime = time.asctime( time.localtime(time.time()) )
		# logging.debug(localtime+ 'comparing with '+row[2])
		
		# #print row[2]
		# ##result = cv2.matchTemplate(nu_arr,template, cv2.TM_CCOEFF)
		# ##imgar = Image.fromarray(nu_arr)
		# result = cv2.matchTemplate(wrd_img, template, cv2.TM_CCORR_NORMED)
		# th, tw = template.shape[:2]
		# ##minVal,maxVal,minLoc,maxLoc = cv.MinMaxLoc(result)
		# ##decide if we have a matching template
		# threshold = 0.97
		# loc = np.where(result >= threshold)
		# match_count = 0
		# for pt in zip(*loc[::-1]):
			# cv2.rectangle(wrd_img, pt, (pt[0] + tw-1, pt[1] + th-2), (255,255,255), -1)
			# match_count+=1
		# if match_count:
			# str = row[1]
			# tAns = str
			# break
		
	if not match_count:
		##lets get this template in database
		#word_match()
		#if debug:
		#	str = store_template(wrd_img)
		#else:
		#	str = 'unknown'
		
		##################################
		##			BOMB SITE HERE		##
		##################################
		cursor = conn.execute("SELECT width,cdata,path,ustat from apfamap order by ROWID")	
		result=0
		str = []
		match_count = 0
		pSet=[]
		cnt = 0
		for row in cursor:
			# cnt = cnt + 1
			# print cnt
			#print DB_PATH
			patth = row[2]
			if ffamily <= 2:
				patth = row[2].replace("dbTemplate",DB_PATH)
			
			template = cv2.imread(patth,0)
			hh,ww = template.shape[:2]

			##If template height is greater than image DON'T DO MATCHING
			if (hh > h) or (ww > w):
				continue
			
			result = cv2.matchTemplate(wrd_img, template, cv2.TM_CCORR_NORMED)
			if row[3] == 0:
				threshold = 0.97
			else:
				threshold = row[3]/100.0
			
			loc = np.where(result >= threshold)
			match_count = 0
			for pt in zip(*loc[::-1]):
				#cv2.rectangle(wrd_img, pt, (pt[0] + tw-1, pt[1] + th-2), (255,255,255), -1)
				str.append( row[1])
				if debug:
					print ("\t\t\t %s @ %d ,%d  :%s"%(row[1].decode('unicode_escape'),pt[0],pt[1],row[2]))
				match_count+=1
				pSet.append(pt[0])
				#print ("Using threshold:%f , %d, %f"%(threshold, row[3], row[3]/100.0))
				
				
			
		rnew = pSet[:]
		pSet.sort()
		#print pSet
		#print rnew
		lval = 0
		
			
		for i in range(len(rnew)):
			#we need to make sure that we are not overwriting the same value
			if((pSet[i]-lval <= 3) and i>0):
				#compare old and current characters
				if (str[rnew.index(pSet[i])] == str[rnew.index(lval)]):
					#its the same character
					#print("%s @ %d matches %s @ %d"%(str[rnew.index(pSet[i])],pSet[i],str[rnew.index(lval)],lval))
					continue
			
			tAns = tAns + str[rnew.index(pSet[i])]
			lval = pSet[i]



		if ffamily == 6:
			nAns = tAns
		if ffamily == 5:
			nAns = tAns
		if ffamily == 4:

			nAns = tAns
		if ffamily == 3:
			nAns = tAns

		if ffamily == 2:
			#values = [i.start() for i in re.finditer(re.escape('dl '),tAns)]
			#nAns = re.sub('T\sh ','Th',tAns)
			#nAns = re.sub('dld','dd ',tAns)
			nAns = re.sub('T\sh','Th',tAns)
			#tAns = re.sub('dl\.','d.',nAns)
			#tAns = re.sub('[a-z]\s[A-Z]','',nAns)
			tAns = re.sub('\s\s',' ',nAns)
			nAns = misd_dot(tAns)
			tAns = re.sub('\s,',',',nAns)
			nAns = re.sub('/\s','/',tAns)
			tAns = un_space(nAns)

		if ffamily == 1:
			#values = [i.start() for i in re.finditer(re.escape('dl '),tAns)]
			nAns = re.sub('dl ','d ',tAns)
			tAns = re.sub('dld','dd ',nAns)
			nAns = re.sub('dl\.','d.',tAns)
			tAns = re.sub('\s\s',' ',nAns)
			
		return tAns
		#return tAns
	else:
		return tAns
		
	return '?undef'

#print parse_data(2,'test_5.jpg')
#print parse_data(1,'test_t5.jpg')