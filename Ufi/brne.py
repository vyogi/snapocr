#This python module will search the Book Reference number in the passed file
import cv2
import numpy as np
from PIL import Image
import os,sys,time,re
from matplotlib import pyplot as plt
import sqlite3

## PATH will hold the address of the sqlite DB
## Sqlite DB will hold the relative path of files\
##	Eg, Suppose the image tempDB is in a folder named\ 
##  img_db in the same director as the sqlite DB\


PATH = "./templateBRNE/"
conn = sqlite3.connect(PATH+'brne.db')

def create_brne_table():
	conn.execute('''CREATE TABLE digits
		(
		width		INTEGER NOT NULL,
		cdata        CHAR(50),
		path        CHAR(50),
		thresh       INTEGER DEFAULT 0,
		ustat         INTEGER DEFAULT 0);''')


def brne_build_db():
	create_brne_table()

	return 0

def get_brne_digits(np_array):
	'''this function tries to find digits in the passed image

	use this functon to fetch the brn
	'''

	brn = 0
	brn = do_training(np_array,False)
	return brn


def get_brne_mat(jpg_file):
	'''This function extracts the last line of the image
		
	return type is a np array.
	Part one is fetching image.
	Part 2 is doing ocr on the minimal area of interest i.e.
	digit string part of last line
	'''

	if(not os.path.isfile(jpg_file)):
		print jpg_file
		sys.exit("Invalid filepath");
	
	## is file a JPG?
	if(not jpg_file.lower().endswith('.jpg')):
		return "Invalid filetype. Current version is limited to jpg file processing.";
	
	## Read image to memory
	jpg_buf = cv2.imread(jpg_file,0)

	## Perform binarization
	ret, th1 = cv2.threshold( jpg_buf, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

	##convert binary image to MAT
	img_mat = np.array(th1)

	rows, cols = img_mat.shape

	# Extract the (last) line containing BookReferenceNumber
	lastLine = np.copy(img_mat[-50:][:])

	# cv2.imshow('lastLine',lastLine)
	# cv2.waitKey()
	# sys.exit(0)
	y_count, x_count = lastLine.shape
	black_flag = 0
	last_line = 0
	first_line = 0
	for xi in xrange(x_count/4,6*(x_count/8)):
		for yi in range(y_count):
			if lastLine[yi][xi]==0: # Indicates a black pixel
				black_flag = 0
				if first_line==0:
					first_line = xi
				else:
					last_line = xi
	    
	    
	# print last_line-first_line
	simg = np.copy(lastLine[ :, first_line-2:last_line+2])
	#cv2.imshow('captured BookReferenceNumber:',simg)
	#cv2.waitKey()
	
	# Lets find consecutive four white lines, parsing from
	# right to left
	y_count, x_count = simg.shape	
	#print 'new dimensions are'
	#print y_count, x_count
	white_count = 0
	blackPixel_flag = 0
	for xi in xrange(x_count-1,0,-1):
		
		blackPixel_flag = 0
		for yi in range(y_count):			
			if simg[yi][xi] == 0: # black pixel
				blackPixel_flag = 1;
				#print ('black at %d,%d'%(xi,yi)) 
				break;
		if blackPixel_flag == 0:
			white_count = white_count + 1
		else:
			white_count = 0
		if white_count >= 5:
			break;
	#print xi, yi, white_count
	#cv2.imshow('captured BookReferenceNumber:',simg[:,xi:])
	#cv2.waitKey()

	# if xi==0:	##Probably the spacing is less
	# ##redo the parse
	# 	y_count, x_count = simg.shape	
	# 	#print 'new dimensions are'
	# 	#print y_count, x_count
	# 	white_count = 0
	# 	blackPixel_flag = 0
	# 	for xi in xrange(x_count-1,0,-1):
			
	# 		blackPixel_flag = 0
	# 		for yi in range(y_count):			
	# 			if simg[yi][xi] == 0: # black pixel
	# 				blackPixel_flag = 1;
	# 				#print ('black at %d,%d'%(xi,yi)) 
	# 				break;
	# 		if blackPixel_flag == 0:
	# 			white_count = white_count + 1
	# 		else:
	# 			white_count = 0
	# 		if white_count >= 4:
	# 			break;

	# if xi==0:	##Probably the spacing is even less
	# ##redo the parse
	# 	y_count, x_count = simg.shape	
	# 	#print 'new dimensions are'
	# 	#print y_count, x_count
	# 	white_count = 0
	# 	blackPixel_flag = 0
	# 	for xi in xrange(x_count-1,0,-1):
			
	# 		blackPixel_flag = 0
	# 		for yi in range(y_count):			
	# 			if simg[yi][xi] == 0: # black pixel
	# 				blackPixel_flag = 1;
	# 				#print ('black at %d,%d'%(xi,yi)) 
	# 				break;
	# 		if blackPixel_flag == 0:
	# 			white_count = white_count + 1
	# 		else:
	# 			white_count = 0
	# 		if white_count >= 3:
	# 			break;
	#cv2.imwrite('working.jpg',img_mat)
	
	img_res =  np.copy(simg[:,xi:])
	### print img_res.shape

	return img_res

def do_matching(wrd_img):
	'''
	To match the digits in a given image
	Tesser engine did perform well. But I will use the matching function. FASTER.
	
	'''
	h,w = wrd_img.shape[:2]
	tAns = ""
	#cursor = conn.execute("SELECT width,cdata,path,ustat from apfamap order by ROWID")
	cursor = conn.execute("SELECT width, cdata, path, thresh from digits order by ROWID	")
	result=0
	str = []
	match_count = 0
	pSet=[]
	cnt = 0
	for row in cursor:
		# cnt = cnt + 1
		# print cnt
		template = cv2.imread(PATH + row[2],0)
		hh,ww = template.shape[:2]

		##If template height is greater than image DON'T DO MATCHING
		if (hh > h) or (ww > w):
			continue
		
		result = cv2.matchTemplate(wrd_img, template, cv2.TM_CCORR_NORMED)
		if row[3] == 0:
			threshold = 0.92
		else:
			threshold = row[3]/100.0
		
		loc = np.where(result >= threshold)
		match_count = 0
		for pt in zip(*loc[::-1]):
			#cv2.rectangle(wrd_img, pt, (pt[0] + tw-1, pt[1] + th-2), (255,255,255), -1)
			str.append( row[1])
			# if debug:
			# 	print ("\t\t\t %s @ %d ,%d  :%s"%(row[1].decode('unicode_escape'),pt[0],pt[1],row[2]))
			match_count+=1
			pSet.append(pt[0])
			#print ("Using threshold:%f , %d, %f"%(threshold, row[3], row[3]/100.0))
			
			
		
	rnew = pSet[:]
	pSet.sort()
	#print pSet
	#print rnew
	lval = 0
	
		
	for i in range(len(rnew)):
		#we need to make sure that we are not overwriting the same value
		if((pSet[i]-lval <= 3) and i>0):
			#compare old and current characters
			if (str[rnew.index(pSet[i])] == str[rnew.index(lval)]):
				#its the same character
				#print("%s @ %d matches %s @ %d"%(str[rnew.index(pSet[i])],pSet[i],str[rnew.index(lval)],lval))
				continue
		
		tAns = tAns + str[rnew.index(pSet[i])]
		lval = pSet[i]
	
	return tAns

	return 0
def store_template(image):
	cv2.imshow("Template to be stored",image)
	cv2.waitKey()
	n= raw_input("Please enter character(s) shown :")
	print ('you entered: %s'%(n.decode('unicode_escape')))
	h, w = image.shape[:2]
	cursor = conn.execute('SELECT max(rowid) FROM digits')
	max_id = cursor.fetchone()[0]
	tem_path='/digits/temp_'+str(int(max_id or 0)+1)+'.bmp'

	#cv2.imshow('Template being stored in mem',image[1:-1,1:-1])
	#cv2.waitKey()
	#cv2.imwrite(PATH+'/digits/temp_con.bmp',image)
	cv2.imwrite(PATH+tem_path,image[1:-1,1:-1])

	conn.execute('insert into digits (width,cdata,path) values(?,?,?)',(w-2,n,tem_path))
	conn.commit()
	print "Saved New template in database"
	#print "Testing save template function/module"

	return n

def do_training(training_img):
	'''
	To train the brn extractor. Usage do_training(template_image)

	This function is to be used to builld the dataset 
	for performing digit matching/detection in the 
	BookReferenceNumber snippet image'''

	# if mode == True:
	# 	storage_enable = True
	# else:
	# 	storage_enable = False
	## vars
	#char_count = 0
	ans = ""
#	if storage_enable:
	## Does template exixt
	if(not os.path.isfile(training_img)):
		print training_img
		sys.exit("Invalid path. File not found.");

	## is file a BMP?
	if(not training_img.lower().endswith('.bmp')):
		return "Invalid filetype. Template must be a BMP file.";

	## read image to memory
	img = cv2.imread(training_img,0)

	## convert te image into a numpy array
	im1arr = np.array(img)


	rows, cols = im1arr.shape

	all_lines = []
	line = 0;
	black_pixel_flag = 0;
	prev_region = 0;
	this_region = 0;
	
	for x in range(rows):
		black_pixel_flag=0;
		for y in range(cols):
			if im1arr[x][y]== 0:			# Matches val == 0 of a black pixel
				black_pixel_flag=1;
		if black_pixel_flag == 0: 			# and white_region == 0:#we have a line
			this_region = 0;
		else:
			this_region = 1;

		if prev_region != this_region:
			line = line + 1
			all_lines.append(x)
			prev_region = this_region
		### We have segregated all lines of text
		### now need to fetch the first symbol  
	#print all_lines
	lines = line/2
	i=0
	while lines:
		#print i, all_lines[i], all_lines[i+1]
		#cv2.imshow('Extracted parts',im1arr[all_lines[i]-2:all_lines[i+1]+2,:])
		#x = cv2.waitKey()
		
		b_in = 0 
		w_in = 0

		x_first = 0
		x_second = 0
		for x in range(cols):
			line_black = 0
			for y in range(all_lines[i]-2, all_lines[i+1]+2):
				if im1arr[y][x] == 0:
					line_black = 1;
					if not b_in:
						b_in =1
						x_first = x
					break
 
			if b_in and (not line_black):	#indicates a white line
				x_second = x
				break	#


		img_data =  np.copy(im1arr[all_lines[i]-2:all_lines[i+1]+2,x_first-2:x_second+2])
		#cv2.imshow('Extracted component', img_data)
		#cv2.waitKey()
		
		#print x_first, x_second		
		#cv2.imshow('Extracted Comonent', im1arr[all_lines[i]-2:all_lines[i+1]+2,x_first-2:x_second+2])
		#cv2.waitKey()
		
		##	we have the co-ordinates of the symbol encoded in the training image-set
		#	now We shall test if a pre-Existing template matches it. If there is no 
		#	matching template we store this current template into memory 
		h,w = img_data.shape[:2]
		## 	summon the template SET
		cursor = conn.execute("SELECT width, cdata, path, thresh from digits order by ROWID")	
		## 	var
		match_flag = False
		result = 0
		##	run match for each template until a match is found
		for row in cursor:
			match_flag = False

			template = cv2.imread(PATH + row[2],0)
			hh,ww = template.shape[:2]

			##If template height is greater than image DON'T DO MATCHING
			if (hh > h) or (ww > w):
				continue

			
			# plt.subplot(121),plt.imshow(img_data,cmap = 'gray')
			# plt.yticks(range(20), rotation=30, size='small')
			# plt.grid(True)
			# plt.title('Input Image'), plt.xticks([]), plt.yticks([])

			# plt.subplot(122),plt.imshow(template,cmap = 'gray')
			# plt.yticks(range(20), rotation=30, size='small')
			# plt.grid(True)
			# plt.title('Input Template'), plt.xticks([]), plt.yticks([])
			# plt.suptitle("Performing match (Images are on same scale)")
			
			# plt.show()


			# n= raw_input("Want to save these images? :")
			# n = n.strip()
			# if n =='Y':
			# 	cv2.imwrite(PATH+'c1.bmp',img_data)
			# 	cv2.imwrite(PATH+'c2.bmp',template)
			# RUN the Matching Program
			#result = cv2.matchTemplate(img_data, template, cv2.TM_CCOEFF_NORMED)
			result = cv2.matchTemplate(img_data, template, cv2.TM_CCORR_NORMED)

			# if row[3] == 0:
			# 	thr = 0.98
			# else:
			# 	thr = row[3]/100
			thr = 0.92
			
			# Find the matches
			loc = np.where(result >= thr)
			#if loc:
			for pt in zip(*loc[::-1]):
				## We have a match lets ask user if he agrees to what is detected
				#print("Character Matches %s"%(row[1]))
				match_flag = True
				break
			if match_flag:
				break
		if match_flag == False:			
			
			s = store_template(img_data)
			print ("New template created %s"%(s))

		else:
			ans += row[1]
			# plt.subplot(121),plt.imshow(img_data,cmap = 'gray')
			# plt.title('Input'), plt.xticks([]), plt.yticks([])
			# plt.subplot(122),plt.imshow(template,cmap = 'gray')
			# plt.title('Matching Template'), plt.xticks([]), plt.yticks([])
			# plt.suptitle("Is this a valid Match")
			# plt.show()

		i += 2
		lines -= 1 
	


	## Each line will have 1 char/digit
	

	## Try to match it with existing pool of images.
	## Does it match?
	#### if YES
	###### continue
	#### else
	###### Store it in folder + Create dB entry

	#char_count = len(ans)
	#print ans
	return ans

if __name__ == '__main__':
	#cv2.imshow('got it',get_brne_mat('test.jpg'))
	#cv2.waitKey()
	print do_training('train.bmp')
	#brne_build_db()