########## Specific objective ###########


## Increase accuracy: Remove prefix 1px white_column from images

import os, collections
import numpy as np
import cv2 

for file_obj in os.listdir("./"):
	if file_obj.endswith(".bmp"):
		## Check if first column is white
		bmpfile = cv2.imread(file_obj,0)
		row,col = bmpfile.shape[:2]
		print row,col,file_obj


		cFlag = False
		for i in xrange(row):
			# bmpfile[i,0] = 133; 
			if bmpfile[i,0] == 0:
				cFlag = True 
				break
		# cv2.imshow("Modified Array",bmpfile)
		# cv2.waitKey()
		if not cFlag:
			print "positive\r\n"
			cv2.imwrite(file_obj,bmpfile[:,1:])

