##############################################
##			snapOCR Programme V1.0			##
##											##
##  Author : Vivek Yogi (vvyogi@gmail.com)	##
##############################################

import os,sys,re
from log_mgr import log_msg
from font_detect import detect_font_family
from brn_extractor import get_digit_str
from docwork import doc_inflate_body
from snapParser import parse_data
from docx import Document
from docx.shared import Pt
from retry import clean_misdDot
## current directory
PATH = "./"

def do_ocr(filename, font_type):
	log_msg("file conversion initiated for file $" + filename + '$')
	
	## file credibility
	if(not os.path.isfile(filename)):
		log_msg("Error $File Path Error$")
		return 0,"File does not exist"
	
	## is file a JPG?
	if(not filename.lower().endswith('.jpg')):
		log_msg("Invalid filetype. Current version is limited to jpg file processing.")
		return 0, "Wrong File Type"
	## which font is used
	# font_type = detect_font_family( filename )
	#font_type = 1
	log_msg( "font family detected:" + str(font_type) )
	
	
	## fetch the BookReferenceNumber
	brne = get_digit_str( filename )
	# brne = '234567'
	log_msg( "BookReferenceNumber :" + str(brne) )

	## parse all data
	mystr = parse_data(font_type, filename)
	log_msg('parse count:' + str(len(mystr)) )

	document = Document(PATH + '/local/' +'outline.docx');
	run = document.add_paragraph().add_run()
	font = run.font
	font.size = Pt(8)
	if font_type==4:
		var = mystr
		nvar = clean_misdDot(var)
		var = re.sub('Countr[^a-z]\s','Country',nvar)
		nvar = re.sub('yy','y',var)
		var = re.sub('HH','H',nvar)
		var = re.sub('hh','h',nvar)
		mystr = var


    
    
	document.add_paragraph(mystr.decode('unicode_escape'))
	document.save('temp.docx');

	doc_inflate_body(filename,"",brne);
	log_msg('DOCUMENT created. Process completed')
	## remove inconsistencies
	##mystr = postprocess(mystr)