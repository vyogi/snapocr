import zipfile, os, sys

# PATH = "/home/vvy/ocrServe/"
# current path
PATH = './'

def doc_inflate_body(filename,dbody,dfooter):
    #uStr = dbody.decode('unicode_escape')
    filename = filename.split('.')[0]

    if not 'docx' in filename:
        filename = filename + '.docx'

    if os.path.isfile(filename):
        os.remove(filename)

    templateDocx = zipfile.ZipFile(PATH+"temp.docx")
    newDocx = zipfile.ZipFile(filename, "a")

    tempXmlStr=""
    with open(templateDocx.extract("word/document.xml", PATH + "temp")) as tempXmlFile:
        tempXmlStr = tempXmlFile.read()
    with open(templateDocx.extract("word/footer2.xml", PATH + "temp")) as tempXmlFile2:
        tempXmlStr2 = tempXmlFile2.read()

    #tempXmlStr = tempXmlStr.replace("$$_body_$$",dbody.decode('unicode_escape').encode('utf-8'))
    tempXmlStr = tempXmlStr.replace("$$_body_$$","")
    tempXmlStr2 = tempXmlStr2.replace("Book Reference Number","Book Reference Number "+dfooter)

    with open( PATH + "temp/temp.xml", "w+") as tempXmlFile:
        tempXmlFile.write(tempXmlStr)

    with open(PATH + "temp/temp2.xml", "w+") as tempXmlFile2:
        tempXmlFile2.write(tempXmlStr2)

    for file in templateDocx.filelist:
        if not ((file.filename == "word/document.xml") or (file.filename == "word/footer2.xml")):
            newDocx.writestr(file.filename, templateDocx.read(file))

    newDocx.write(PATH+"temp/temp.xml", "word/document.xml")
    newDocx.write(PATH+"temp/temp2.xml", "word/footer2.xml")

    templateDocx.close()
    newDocx.close()

    return 0

myStr = 'loop\u00e9r is known f\u00f5r the clean job get it \u00e8 \u00ff'

doc_inflate_body('chillMan',myStr,'123456')
