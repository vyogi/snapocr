README
------

Before running the program fetch these softwares and install these

python 2.7.10       https://www.python.org/downloads/
openCV 2.4.11       http://opencv.org/downloads.html
PIL                 http://effbot.org/media/downloads/PIL-1.1.7.win32-py2.7.exe
scipy               http://sourceforge.net/projects/scipy/files/
numpy               http://www.lfd.uci.edu/~gohlke/pythonlibs/
matplotlib          http://sourceforge.net/projects/matplotlib/files/matplotlib/matplotlib-1.3.1/matplotlib-1.3.1.win32-py2.7.exe/download?use_mirror=nchc         

This one needs a different approach 
-----------------------------------
python-docx         https://codeload.github.com/mikemaccana/python-docx/zip/master
        This package comes in a zip file.
        1. Extract it in a fresh folder. Eg D:\MyFolder\pythonDocx
        2. open command promt and take it to the directory where you have extracted the above things
        3. >>python setup.py install
        4. Done

To make sure everything is properly installed
----------------------------------------------
1. Open command prompt and type
    >> python -c "import cv2; import numpy as np; from PIL import Image; import sqlite3; from docx import Document;"
2. If it runs silently with no output then ALL is SET PROPERLY otherwise there is a installation problem.



How to run the software
------------------------

1. Extract the program into the folder containing the images
2. Open command promt and check into the directory where you have extracted the content
3. Type
    >> python crawl.py [1-5]
    and press Enter.
    
    The program will execute. Some messages will be printed